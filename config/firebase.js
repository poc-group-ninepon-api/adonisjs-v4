'use strict'

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')
const admin = require("firebase-admin");

module.exports = {
  config: {
    databaseURL: "firebase-adminsdk-ucrix@gitlab-ninepon-poc.iam.gserviceaccount.com",
    credential: admin.credential.cert({
      type: "service_account",
      project_id: Env.get('FIREBASE_project_id'),
      private_key_id: Env.get('FIREBASE_private_key_id'),
      private_key: Env.get('FIREBASE_private_key', '').replace(/\\n/g, '\n'),
      client_email: Env.get('FIREBASE_client_email'),
      client_id: Env.get('FIREBASE_client_id'),
      auth_uri: "https://accounts.google.com/o/oauth2/auth",
      token_uri: "https://oauth2.googleapis.com/token",
      auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
      client_x509_cert_url: Env.get('FIREBASE_client_x509_cert_url')
    })
  }
}
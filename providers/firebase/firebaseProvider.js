'use strict'

const { ServiceProvider } = require('@adonisjs/fold')
const admin = require("firebase-admin");

class firestoreProvider extends ServiceProvider {
   register () {
   this.app.singleton('ninepon/firebase', () => {
      const config = this.app.use('Adonis/Src/Config')
      admin.initializeApp(config.get('firebase.config'));
      return admin
    })
  }

  boot () {
    //
  }
}

module.exports = firestoreProvider

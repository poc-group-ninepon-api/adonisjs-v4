'use strict'
const { validate, formatters } = use('Validator')
const firebase = use('ninepon/firebase')
const firestore = firebase.firestore();

class UsersController {
    async index ({ params, request, response }) {
        const user = await firestore.collection('users').doc(`${params.username}`).get()
        let res = user.data()
        if(!res){
            return response.status(404).json({'message': `username ${params.username} has not found`})
        }
        return response.status(200).json(res)
    }

    async store({ params, request, response }) {
        const rules = {
            username: 'required|string',
            password: 'required|string',
            fullName: 'required|string',
            telephoneNumber: 'required|string'
        }
        const validation = await validate(request.all(), rules, null, formatters.JsonApi)
        if (validation.fails()) {
            return response.status(422).json(validation.messages())
        }

        let username = request.input('username')
        const user = firestore.collection('users').doc(`${username}`)
        let getDataUser = await user.get()
        if(getDataUser.data()){
            return response.status(409).json({'message': 'dplicated user'})
        }

        let datastore = request.only(['username', 'password', 'fullName', 'telephoneNumber'])
        let resultAdduser = await user.set( datastore , {merge: false})
            .then((docRef) => { return true })
            .catch((error) => { return false })

        if(resultAdduser){
            return response.status(200).json({'message': 'added user successful'}) 
        }else{
            return response.status(409).json({'message': 'added user has failed'}) 
        }
    }

    async destroy({ params, request, response }) {
        const user = firestore.collection('users').doc(`${params.username}`)
        let getDataUser = await user.get()
        if(!getDataUser.data()){
            return response.status(404).json({'message': `username ${params.username} has not found`})
        }
        let resultDeleteuser = await user.delete()
            .then((docRef) => { return true })
            .catch((error) => { return false })

        if(resultDeleteuser){
            return response.status(200).json({'message': 'removed user successful'}) 
        }else{
            return response.status(409).json({'message': 'removed user has failed'}) 
        }
    }

    async update({ params, request, response }) {
        const rules = {
            username: 'required|string',
            password: 'required|string',
            fullName: 'required|string',
            telephoneNumber: 'required|string'
        }
        const validation = await validate(request.all(), rules, null, formatters.JsonApi)
        if (validation.fails()) {
            return response.status(422).json(validation.messages())
        }

        let username = request.input('username')
        const user = firestore.collection('users').doc(`${username}`)
        let getDataUser = await user.get()
        if(!getDataUser.data()){
            return response.status(404).json({'message': `username ${username} has not found`})
        }

        let datastore = request.only(['username', 'password', 'fullName', 'telephoneNumber'])
        let resultUpdateuser = await user.update(datastore)
            .then((docRef) => { return true })
            .catch((error) => { return false })

        if(resultUpdateuser){
            return response.status(200).json({'message': 'updated user successful'}) 
        }else{
            return response.status(409).json({'message': 'updated user has failed'}) 
        }
    }
}

module.exports = UsersController
